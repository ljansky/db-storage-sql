const sqlAdapter = require('../lib/sqlAdapter');

beforeEach(async function () {
	this.connection = sqlAdapter.getDbConnection({
		host: 'localhost',
		user: 'root',
		password: 'password',
		database: 'db'
	});

	this.adapter = sqlAdapter.createAdapter(this.connection);
});