const knexFactory = require('knex');
const R = require('ramda');

const findByIdFilter = id => ([{
	operator: 'eq',
	field: 'id',
	value: id
}]);

const setQuery = (baseTable, or, relations, usedRelations) => (q, filter) => {
	const whereFunction = or ? 'orWhere' : 'where';
	const fieldWithPrefix = `${ baseTable }.${ filter.field}`;
	const relation = filter.field && filter.field !== '' && relations.find(rel => rel.name === filter.field);

	const operators = {
		eq: q => q[whereFunction](fieldWithPrefix, '=', filter.value),
		in: q => q[whereFunction](fieldWithPrefix, 'in', filter.value),
		gt: q => q[whereFunction](fieldWithPrefix, '>', filter.value),
		gte: q => q[whereFunction](fieldWithPrefix, '>=', filter.value),
		lt: q => q[whereFunction](fieldWithPrefix, '<', filter.value),
		lte: q => q[whereFunction](fieldWithPrefix, '<=', filter.value),
		and: q => q[whereFunction](function() {
			setWhereParams(baseTable, filter.value, relation ? relation.relations : relations, usedRelations, false, relation)(this)
		}),
		or: q => q[whereFunction](function() {
			setWhereParams(baseTable, filter.value, relation ? relation.relations : relations, usedRelations, true, relation)(this)
		}),
		has: q => q[whereFunction](function() {
			setWhereParams(baseTable, filter.value, relation ? relation.relations : relations, usedRelations, false, relation)(this)
		}),
		like: q => q[whereFunction](fieldWithPrefix, 'like', `%${filter.value}%`)
	};

	return operators[filter.operator](q);
}

const setJoinParams = (baseTable, relations) => query => {
	return R.reduce((query, relation) => {
		query.leftJoin(relation.table, baseTable + '.' + relation.ownField, relation.table + '.' + relation.foreignField);
		if (relation.relations && relation.relations.length) {
			query = setJoinParams(relation.table, relation.relations)(query);
		}

		return query;
	}, query)(relations);
};

const setWhereParams = (tableName, filters, relations, usedRelations = [], or = false, joinRelation = null) => query => {
	const setQueryFunction = setQuery(joinRelation ? joinRelation.table : tableName, or, relations, usedRelations);
	return R.reduce(setQueryFunction, query, filters);
};

const setRelatedData = async (db, data, relations) => {
	let resData = data;

	for (let relation of relations) {

		const foreignIds = R.map(R.prop(relation.ownField), data);
		const relatedData = await find(db, relation.table)({
			filter: [{
				operator: 'in',
				field: relation.foreignField,
				value: foreignIds
			}],
			relations: relation.relations
		});

		const findRelated = relation.hasMany ? R.filter : R.find;
		resData = R.map(item => {
			const relData = findRelated(rIt => rIt[relation.foreignField] === item[relation.ownField], relatedData);
			return R.assoc(relation.name, relData, item);
		}, resData);
	}
	
	return resData;
};

const find = (db, tableName) => async (params = {}) => {
	const filter = params.filter || [];
	const limit = params.limit || 0;
	const offset = params.offset || 0;
	const order = params.order || [];
	const relations = params.relations || [];

	let query = db.select(tableName + '.*')
		.distinct(tableName + '.id')
		.from(tableName)
	
	if (limit) {
		query = query.limit(limit);
	}

	if (offset) {
		query = query.offset(offset);
	}

	for (let orderParam of order) {
		query = query.orderBy(orderParam.by, orderParam.desc ? 'desc' : 'asc');
	}

	query = setJoinParams(tableName, relations)(query);
	const q = setWhereParams(tableName, filter, relations)(query);

	const foundData = await q;
	const withRelations = await setRelatedData(db, foundData, relations);

	return withRelations;
};

const insertOne = (db, tableName) => data => {
	return db(tableName).insert([data])
		.then(idData => {
			return find(db, tableName)({ filter: findByIdFilter(idData['0']) });
		})
		.then(data => {
			return data[0] || null;
		})
};

const updateOneById = (db, tableName) => (id, data) => {
	return db(tableName)
		.where('id', '=', id)
		.update(data)
		.then(() => {
			return find(db, tableName)({ filter: findByIdFilter(id) });
		})
		.then(data => {
			return data[0] || null;
		})
};

const deleteOneById = (db, tableName) => async (id) => {
	const entityToDelete = await find(db, tableName)({ filter: findByIdFilter(id) });
	if (entityToDelete[0]) {
		await db(tableName)
			.where('id', '=', id)
			.del();
	}

	return entityToDelete[0] || null;
};

const upsertFixture = (db, tableName) => fixtureData => {
	return fixtureData;
};

const createAdapter = db => {

	return tableName => ({
		find: find(db, tableName),
		insertOne: insertOne(db, tableName),
		updateOneById: updateOneById(db, tableName),
		deleteOneById: deleteOneById(db, tableName),
		upsertFixture: upsertFixture(db, tableName)
	});
};

const getDbConnection = connectionInfo => {
	const knex = knexFactory({
		client: 'mysql',
		debug: connectionInfo.debug || false,
		connection: {
			host: connectionInfo.host,
			user: connectionInfo.user,
			password: connectionInfo.password,
			database: connectionInfo.database,
			charset: connectionInfo.charset ? connectionInfo.charset : 'utf8'
		}
	});

	return knex;
};

module.exports = {
	createAdapter,
	getDbConnection
};